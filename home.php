

<div class="content-wrapper " >
    <div class="row">
      <div class="col-md-12 grid-margin">
        <div class="row">
          <div class="col-12 col-xl-8 mb-4 mb-xl-0">
            <h3 class="font-weight-bold">Welcome <?=$_SESSION['u']?></h3>
            <h6 class="font-weight-normal mb-0">Robot Chatting menjawab setiap kebutuhan kamu agar #memudahkan !</h6>
          </div>
          <div class="col-12 col-xl-4">
            <div class="justify-content-end d-flex">
            <div class="dropdown flex-md-grow-1 flex-xl-grow-0">
              <button class="btn btn-sm btn-light bg-white dropdown-toggle" type="button" id="dropdownMenuDate2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                <i class="mdi mdi-calendar"></i> Today (<?=$today?>)
              </button>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuDate2">
                <a class="dropdown-item" href="#">January - March</a>
                <a class="dropdown-item" href="#">March - June</a>
                <a class="dropdown-item" href="#">June - August</a>
                <a class="dropdown-item" href="#">August - November</a>
              </div>
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 grid-margin stretch-card">
        <div class="card tale-bg">
          <div class="card-people mt-auto">
            <img src="images/dashboard/people.svg" alt="people">

          </div>
        </div>
      </div>
      <div class="col-md-6 grid-margin transparent">
        <div class="row">
          <div class="col-md-6 mb-4 stretch-card transparent">
            <div class="card card-tale">
              <div class="card-body">
                <p class="mb-4">Pesan Masuk</p>
                <p class="fs-30 mb-2">0</p>
              </div>
            </div>
          </div>
          <div class="col-md-6 mb-4 stretch-card transparent">
            <div class="card card-light-danger">
              <div class="card-body">
                <p class="mb-4">Pesan Keluar</p>
                <p class="fs-30 mb-2">0</p>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 mb-4 mb-lg-0 stretch-card transparent">
            <div class="card card-light-blue">
              <div class="card-body">
                <p class="mb-4">Percakapan</p>
                <p class="fs-30 mb-2">0</p>
              </div>
            </div>
          </div>


        </div>
      </div>
    </div>
    
    <?php
    if($id_user!="" && ( $tipe_user=="ADMIN" || $tipe_user=="BIRO_JODOH") )
    {
    ?>
    <hr>

    <div class="row">
      <div class="col-md-12 grid-margin transparent">
          <div class="row">
            <div class="col-md-4 mb-4 stretch-card transparent">
              <div class="card card-tale">
                <div class="card-body">
                  <p class="mb-4">Pendaftar Laki-Laki</p>
                  <p class="fs-30 mb-2">4006</p>
                  <!-- <p>10.00% (30 days)</p> -->
                </div>
              </div>
            </div>
            <div class="col-md-4 mb-4 stretch-card transparent">
              <div class="card card-dark-blue">
                <div class="card-body">
                  <p class="mb-4">Pendaftar Perempuan</p>
                  <p class="fs-30 mb-2">61344</p>
                  <!-- <p>22.00% (30 days)</p> -->
                </div>
              </div>
            </div>
            <div class="col-md-4 mb-4 stretch-card transparent">
              <div class="card card-light-danger">
                <div class="card-body">
                  <p class="mb-4">Pasangan Match</p>
                  <p class="fs-30 mb-2">61344</p>
                  <!-- <p>22.00% (30 days)</p> -->
                </div>
              </div>
            </div>
          </div>

          <div class="row justify-content-center">
            <div class="col-md-4 mb-4 mb-lg-0 stretch-card transparent">
              <div class="card card-light-blue">
                <div class="card-body">
                  <p class="mb-4">Daftar Pendaftar</p>
                  <p class="fs-30 mb-2">34040</p>
                  <!-- <p>2.00% (30 days)</p> -->
                </div>
              </div>
            </div>
            <div class="col-md-4 stretch-card transparent">
              <div class="card card-tale">
                <div class="card-body">
                  <p class="mb-4">Daftar Pasangan Match</p>
                  <p class="fs-30 mb-2">47033</p>
                  <!-- <p>0.22% (30 days)</p> -->
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>

    <?php
    }
    ?>
</div>