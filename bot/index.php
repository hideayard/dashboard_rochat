<?php
session_start();
// error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once ('../config/MysqliDb.php');
include_once ("../config/db.php");
$db = new MysqliDb ('localhost', $dbuser, $dbpass, $dbname);
include("../config/functions.php");
require_once ("../jwt_token.php");


$botname=isset($_GET['botname']) ? $_GET['botname'] : "default"; 
$sql = "SELECT * FROM bot WHERE bot_name = '".$botname."' "; 
$result = $db->rawQuery($sql);//@mysql_query($sql);

$bot_foto = "../images/chat2.png";
$user_foto = "../images/user_icon.png";
$bot_wall = null;
$bot_name ="Rochat Bot";

if(count($result)<=0)
{
  $bot_title = "Ro Bot";
  $opening = "Hi, welcome to Rochat! Go ahead and send me a message. 😄";
}
else{
    $bot_name = $result[0]["bot_name"];
    $bot_title = $result[0]["bot_title"];
    $opening = $result[0]["bot_opening"];
    $bot_foto = isset($result[0]["bot_foto"])?"../uploads/bot/".$result[0]["bot_foto"]:"../images/chat2.png";
    $bot_wall = isset($result[0]["bot_wall"])?"../uploads/bot/".$result[0]["bot_wall"]:null;
}

date_default_timezone_set("Asia/Jakarta");

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex, nofollow">
    <link rel="shortcut icon" href="../images/rochat_icon.png" />

    <title><?=$bot_title?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <style type="text/css">
    	body,html{
			height: 100%;
			margin: 0;
            <?php
            if($bot_wall)
            {
                echo 'background-image: url("'.$bot_wall.'");
                background-color: #7F7FD5;';
            }
            else{
               echo "background: #7F7FD5;
               background: -webkit-linear-gradient(to right, #91EAE4, #86A8E7, #7F7FD5);
               background: linear-gradient(to right, #91EAE4, #86A8E7, #7F7FD5);"; 
            }
            ?>
			/* background: #7F7FD5;
	        background: -webkit-linear-gradient(to right, #91EAE4, #86A8E7, #7F7FD5);
	        background: linear-gradient(to right, #91EAE4, #86A8E7, #7F7FD5); */
		}

		.chat{
			margin-top: auto;
			margin-bottom: auto;
		}
		.card{
            height: 100vh;
			border-radius: 15px !important;
			background-color: rgba(0,0,0,0.4) !important;
		}
		.contacts_body{
			padding:  0.75rem 0 !important;
			white-space: nowrap;
		}
		.msg_card_body{
			overflow-y: scroll;
            min-height: 400px;
            scrollbar-color: #d4aa70 #e4e4e4;
            scrollbar-width: thin;
            direction: ltr;
		}
        .msg_card_body::-webkit-scrollbar {
        width: 20px;
        }

        .msg_card_body::-webkit-scrollbar-track {
        background-color: #e4e4e4;
        border-radius: 100px;
        }

        .msg_card_body::-webkit-scrollbar-thumb {
        border-radius: 100px;
        border: 6px solid rgba(0, 0, 0, 0.18);
        border-left: 0;
        border-right: 0;
        background-color: #708bd4;
        }
        .msg_card_body::-webkit-scrollbar-thumb:hover {
        background: #505eab; 
        }
		.card-header{
			border-radius: 15px 15px 0 0 !important;
			border-bottom: 0 !important;
		}
	 .card-footer{
		border-radius: 0 0 15px 15px !important;
			border-top: 0 !important;
	}
		.container{
			align-content: center;
		}
		.search{
			border-radius: 15px 0 0 15px !important;
			background-color: rgba(0,0,0,0.3) !important;
			border:0 !important;
			color:white !important;
		}
		.search:focus{
		     box-shadow:none !important;
           outline:0px !important;
		}
		.type_msg{
			background-color: rgba(0,0,0,0.3) !important;
			border:0 !important;
			color:white !important;
			height: 60px !important;
			overflow-y: auto;
		}
			.type_msg:focus{
		     box-shadow:none !important;
           outline:0px !important;
		}
		.attach_btn{
	border-radius: 15px 0 0 15px !important;
	background-color: rgba(0,0,0,0.3) !important;
			border:0 !important;
			color: white !important;
			cursor: pointer;
		}
		.send_btn{
	border-radius: 0 15px 15px 0 !important;
	background-color: rgba(0,0,0,0.3) !important;
			border:0 !important;
			color: white !important;
			cursor: pointer;
		}
		.search_btn{
			border-radius: 0 15px 15px 0 !important;
			background-color: rgba(0,0,0,0.3) !important;
			border:0 !important;
			color: white !important;
			cursor: pointer;
		}
		.contacts{
			list-style: none;
			padding: 0;
		}
		.contacts li{
			width: 100% !important;
			padding: 5px 10px;
			margin-bottom: 15px !important;
		}
	.active{
			background-color: rgba(0,0,0,0.3);
	}
		.user_img{
			height: 70px;
			width: 70px;
			border:1.5px solid #f5f6fa;
		
		}
		.user_img_msg{
			height: 40px;
			width: 40px;
			border:1.5px solid #f5f6fa;
		
		}
	.img_cont{
			position: relative;
			height: 70px;
			width: 70px;
	}
	.img_cont_msg{
			height: 40px;
			width: 40px;
	}
	.online_icon{
		position: absolute;
		height: 15px;
		width:15px;
		background-color: #4cd137;
		border-radius: 50%;
		bottom: 0.2em;
		right: 0.4em;
		border:1.5px solid white;
	}
	.offline{
		background-color: #c23616 !important;
	}
	.user_info{
		margin-top: auto;
		margin-bottom: auto;
		margin-left: 15px;
	}
	.user_info span{
		font-size: 20px;
		color: white;
	}
	.user_info p{
	font-size: 10px;
	color: rgba(255,255,255,0.6);
	}
	.video_cam{
		margin-left: 50px;
		margin-top: 5px;
	}
	.video_cam span{
		color: white;
		font-size: 20px;
		cursor: pointer;
		margin-right: 20px;
	}
	.msg_cotainer{
        min-width: 100px;
		margin-top: auto;
		margin-bottom: auto;
		margin-left: 10px;
		border-radius: 25px;
		background-color: #82ccdd;
		padding: 10px;
		position: relative;
	}
	.msg_cotainer_send{
        min-width: 70px;
		margin-top: auto;
		margin-bottom: auto;
		margin-right: 10px;
		border-radius: 25px;
		background-color: #78e08f;
		padding: 10px;
		position: relative;
	}
	.msg_time{
		position: absolute;
		left: 0;
		bottom: -15px;
		color: rgba(255,255,255,0.5);
		font-size: 10px;
	}
	.msg_time_send{
		position: absolute;
		right:0;
		bottom: -15px;
		color: rgba(255,255,255,0.5);
		font-size: 10px;
	}
	.msg_head{
		position: relative;
	}
	#action_menu_btn{
		position: absolute;
		right: 10px;
		top: 10px;
		color: white;
		cursor: pointer;
		font-size: 20px;
	}
	.action_menu{
		z-index: 1;
		position: absolute;
		padding: 15px 0;
		background-color: rgba(0,0,0,0.5);
		color: white;
		border-radius: 15px;
		top: 30px;
		right: 15px;
		display: none;
	}
	.action_menu ul{
		list-style: none;
		padding: 0;
	margin: 0;
	}
	.action_menu ul li{
		width: 100%;
		padding: 10px 15px;
		margin-bottom: 5px;
	}
	.action_menu ul li i{
		padding-right: 10px;
	
	}
	.action_menu ul li:hover{
		cursor: pointer;
		background-color: rgba(0,0,0,0.2);
	}
	@media(max-width: 576px){
	.contacts_card{
		margin-bottom: 15px !important;
	}
	}    </style>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="../vendors/sweetalert2/sweetalert2.min.css">
    <script src="../vendors/sweetalert2/sweetalert2.min.js"></script>
</head>
<body>
    <!DOCTYPE html>
<html>
	<head>
		<title>Chat</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.js"></script>
	</head>

    <body>
		<div class="container-fluid h-100">
			<div class="row justify-content-center h-100">
				
				<div class="col-md-12 col-xl-12 chat">
					<div class="card">
						<div class="card-header msg_head">
							<div class="d-flex bd-highlight">
								<div class="img_cont">
									<img src="<?=$bot_foto?>" class="rounded-circle user_img">
									<span class="online_icon"></span>
								</div>
								<div class="user_info">
									<span><?=$bot_title?></span>
									<p><i id="jml_msg"><?=$jml_msg=1?></i> Messages</p>
								</div>
								
							</div>
                            
							<span id="action_menu_btn"><i class="fas fa-ellipsis-v"></i></span>
							<div class="action_menu">
								<ul>
									<!-- <li><i class="fas fa-user-circle"></i> View profile</li>
									<li><i class="fas fa-users"></i> Add to close friends</li-->
									<li><i class="fas fa-cog"></i> Settings</li> 
									<li><i class="fas fa-window-close"></i> Close</li>
								</ul>
							</div>
						</div>
						<div id="msg_card_body" class="card-body msg_card_body">
							
							
							
							
						</div>
						<div class="card-footer">
							<div class="input-group">
								<div class="input-group-append">
									<span class="input-group-text attach_btn"><i class="fas fa-paperclip"></i></span>
								</div>
								<textarea id="type_msg" name="type_msg" class="form-control type_msg" placeholder="Type your message..." tabindex="1" maxlength='2000'></textarea>
								<div class="input-group-append">
									<span id="send_btn" class="input-group-text send_btn"><i class="fas fa-location-arrow"></i></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
	<script type="text/javascript">
    $(document).ready(function(){

        $('#type_msg').keypress(
        function(e){
            if (e.keyCode == 13) {
                e.preventDefault();
                submitMsg();
            }
        });

        
        $('#action_menu_btn').click(function(){
            $('.action_menu').toggle();
        });

        const el = document.getElementById("send_btn");
        el.addEventListener("click", submitMsg, false);

        const BOT_IMG = "<?=$bot_foto?>";
        const PERSON_IMG = "<?=$user_foto?>";
        const BOT_NAME = "<?=$botname?>";
        const PERSON_NAME = "You";
        const msgerChat = get(".msg_card_body");

        appendMessage('<?=$botname?>', '<?=$bot_foto?>', "left", '<?=$opening?>');

        function removeLineBreak(str) {
            return str.replace(/(\r\n|\n|\r)/gm, " ");
        }
        
        function removeWhitespaces(string, i = 0, res = "") {
        if (i >= string.length)
            return res
        else
        if (string[i] == " ")
            return removeWhitespaces(string, i + 1, res)
        else
            return removeWhitespaces(string, i + 1, res += string[i])
        }

        function submitMsg()
        {
            console.log("submit msg");
            let msgText = document.getElementById("type_msg").value;
            // if(removeWhitespaces(msgText) != "")
            if($("#type_msg").val().trim().length >= 1)
            {
                
                var data = "title=<?=$bot_title?>&bot=<?=$botname?>&msg="+msgText+"&token="+getCookie('token');
                console.log(PERSON_NAME, PERSON_IMG, "right", msgText);
                appendMessage(PERSON_NAME, PERSON_IMG, "right", msgText);

                $.ajax({
                type : 'POST',
                url  : 'actionchatbot.php',
                data : data,//+ '&token=' + getCookie('token'),
                beforeSend: function()
                {   
                    // $("#btn-faq").html('<i class="fa fa-sync fa-spin"></i> &nbsp; Processing');
                },
                success :  function(response)
                    {          
                        try{
                            rv = JSON.parse(response);
                            if(isEmpty(rv) || rv.status==false)
                            {
                                Swal.fire(
                                    'error!',
                                    'Error NO DATA, '+rv.messages,
                                    'error'
                                    );
                                console.log("NO DATA : ", response);
                                        
                            }
                            else
                            {
                                if(rv.status==true)
                                {
                                
                                ///send reponse bot
                                appendMessage(BOT_NAME, BOT_IMG, "left", rv.messages);
                                // botResponse(rv.messages);
                                // Swal.fire(
                                //   'Success!',
                                //   'Success Submit Data!',
                                //   'success'
                                //   );
                                console.log("SUCCESS : ", rv);
                                // setTimeout(function(){ window.location="faq"; }, 1000);
                                // window.location="index.php";
                                }

                            }
                        }   
                        catch (e) {
                        
                        Swal.fire(
                                    'error!',
                                    'Error Input Data, '+e,
                                    'error'
                                    );
                                    // $("#btn-faq").html('<i class="fa fa-sync fa-spin"></i> &nbsp; Simpan');
                        
                    console.log("ERROR : ", e);

                        }           
                        
                    },
                timeout: 3000 // sets timeout to 3 seconds
                });
            }
            else
            {
                console.log("pesan masih kosong");
            }
            // console.log("val=",$('#type_msg').val());
            $('#type_msg').val('');
    
        }

        function appendMessage(name, img, side, text) {
            let d = new Date();
            // var datestring = ("0" + d.getDate()).slice(-2) + "-" + ("0"+(d.getMonth()+1)).slice(-2) + "-" + d.getFullYear() + " " + ("0" + d.getHours()).slice(-2) + ":" + ("0" + d.getMinutes()).slice(-2);
            // console.log("asli="+datestring);
            let jamString = new Date().toLocaleTimeString().replace(/([\d]+:[\d]{2})(:[\d]{2})(.*)/, "$1$3");
            console.log(jamString)

            const kiri = `<div class="d-flex justify-content-start mb-4">
								<div class="img_cont_msg">
									<img src="<?=$bot_foto?>" class="rounded-circle user_img_msg">
								</div>
								<div class="msg_cotainer">
									`+text+`
									<span class="msg_time"><?=$bot_name?> - `+jamString+`</span>
								</div>
							</div>`;
            const kanan = `<div class="d-flex justify-content-end mb-4">
								<div class="msg_cotainer_send">
                                    `+text+`
									<span class="msg_time_send">You - `+jamString+`</span>
								</div>
								<div class="img_cont_msg">
							<img src="<?=$user_foto?>" class="rounded-circle user_img_msg">
								</div>
							</div>`;
            
            if(side == 'right')
            {
                msgerChat.insertAdjacentHTML("beforeend", kanan);
            }
            else{
                msgerChat.insertAdjacentHTML("beforeend", kiri);
            }
            msgerChat.scrollTop += 500;
        }

        function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
            }
            return "";
        }

        function botResponse() {
            const r = random(0, BOT_MSGS.length - 1);
            const msgText = BOT_MSGS[r];
            const delay = msgText.split(" ").length * 100;

            setTimeout(() => {
                appendMessage(BOT_NAME, BOT_IMG, "left", msgText);
            }, delay);
        }

        // Utils
        function get(selector, root = document) {
            return root.querySelector(selector);
        }

        function isEmpty(obj) {
            for(var prop in obj) {
                if(obj.hasOwnProperty(prop))
                    return false;
            }
            return true;
        }
        

	});	
    </script>
</body>
</html>
