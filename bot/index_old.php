<?php
session_start();
// error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once ('../config/MysqliDb.php');
include_once ("../config/db.php");
$db = new MysqliDb ('localhost', $dbuser, $dbpass, $dbname);
include("../config/functions.php");
require_once ("../jwt_token.php");


$botname=isset($_GET['botname']) ? $_GET['botname'] : "default"; 
$sql = "SELECT * FROM bot WHERE bot_name = '".$botname."' "; 
$result = $db->rawQuery($sql);//@mysql_query($sql);
// var_dump($result);


if(count($result)<=0)
{
  $bot_title = "Ro Bot";
  $opening = "Hi, welcome to SimpleChat! Go ahead and send me a message. 😄";
}
else{
  $bot_title = $result[0]["bot_title"];
  $opening = $result[0]["bot_opening"];
}
date_default_timezone_set("Asia/Jakarta");

?>
<!DOCTYPE html>
<html>
    <head>
    <title>Rochat Bot</title>
    <link rel="shortcut icon" href="../images/rochat_icon.png" />

    <link rel="stylesheet" href="bot1.css">

<script src="../js/jquery/jquery.min.js"></script>
<!-- SweetAlert2 -->
<link rel="stylesheet" href="../vendors/sweetalert2/sweetalert2.min.css">
<script src="../vendors/sweetalert2/sweetalert2.min.js"></script>
<script>
function isEmpty(obj) {
    for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
            return false;
    }
      return true;
  }
  
  function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }

  function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }
  </script>
</head>
    <body>

            
        <section class="msger">
        <header class="msger-header">
            <div class="msger-header-title">
            <i class="fas fa-comment-alt"></i> <?=$bot_title?>
            </div>
            <div class="msger-header-options">
            <span><i class="fas fa-cog"></i></span>
            </div>
        </header>

        <main class="msger-chat">
            <div class="msg left-msg">
            <div
            class="msg-img"
            style="background-image: url(https://image.flaticon.com/icons/svg/327/327779.svg)"
            ></div>

            <div class="msg-bubble">
                <div class="msg-info">
                <div class="msg-info-name"><?=$botname?></div>
                <div class="msg-info-time"><?=date('H:i')?></div>
                </div>

                <div class="msg-text">
                <?=$opening?>
                </div>
            </div>
            </div>

        </main>

        <form class="msger-inputarea">
            <input type="text" class="msger-input" placeholder="Enter your message...">
            <button type="submit" class="msger-send-btn">Send</button>
        </form>
        </section>

<script>
$(document).ready(function () {

    const msgerForm = get(".msger-inputarea");
    const msgerInput = get(".msger-input");
    const msgerChat = get(".msger-chat");

    const BOT_MSGS = [
      "Hi, how are you?",
      "Ohh... I can't understand what you trying to say. Sorry!",
      "I like to play games... But I don't know how to play!",
      "Sorry if my answers are not relevant. :))",
      "I feel sleepy! :("
    ];

    // Icons made by Freepik from www.flaticon.com
    const BOT_IMG = "https://image.flaticon.com/icons/svg/327/327779.svg";
    const PERSON_IMG = "https://image.flaticon.com/icons/svg/145/145867.svg";
    const BOT_NAME = "<?=$botname?>";
    const PERSON_NAME = "You";

    msgerForm.addEventListener("submit", event => {
      event.preventDefault();

      const msgText = msgerInput.value;
      if (!msgText) return;

      // send ajax
      submitMsg(msgText);
      
    });
    function submitMsg(msgText)
    {
      appendMessage(PERSON_NAME, PERSON_IMG, "right", msgText);
      msgerInput.value = "";

      var data = "title=<?=$bot_title?>&bot=<?=$botname?>&msg="+msgText+"&token="+getCookie('token');
      // console.log("token = ", getCookie('token'));
      // console.log("msg = ", msgText );
      
      $.ajax({
      type : 'POST',
      url  : 'actionchatbot.php',
      data : data,//+ '&token=' + getCookie('token'),
      beforeSend: function()
      {   
          // $("#btn-faq").html('<i class="fa fa-sync fa-spin"></i> &nbsp; Processing');
      },
      success :  function(response)
          {          
            try{
                  rv = JSON.parse(response);
                  if(isEmpty(rv) || rv.status==false)
                  {
                    Swal.fire(
                        'error!',
                        'Error NO DATA, '+rv.messages,
                        'error'
                        );
                      console.log("NO DATA : ", response);
                      // $("#error").fadeIn(500, function(){                        
                      // $("#error").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; Username Or Password is Wrong !</div>');});
                      // $("#btn-faq").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Simpan');
                              
                  }
                  else
                  {
                    if(rv.status==true)
                    {
                      
                      ///send reponse bot
                      appendMessage(BOT_NAME, BOT_IMG, "left", rv.messages);
                      // botResponse(rv.messages);
                      // Swal.fire(
                      //   'Success!',
                      //   'Success Submit Data!',
                      //   'success'
                      //   );
                      console.log("SUCCESS : ", rv);
                      // setTimeout(function(){ window.location="faq"; }, 1000);
                      // window.location="index.php";
                    }

                  }
            }   
            catch (e) {
              
              Swal.fire(
                        'error!',
                        'Error Input Data, '+e,
                        'error'
                        );
                        // $("#btn-faq").html('<i class="fa fa-sync fa-spin"></i> &nbsp; Simpan');
            
          console.log("ERROR : ", e);

            }           
              
        },
      timeout: 3000 // sets timeout to 3 seconds
      });
    }

    function appendMessage(name, img, side, text) {
      //   Simple solution for small apps
      const msgHTML = `
        <div class="msg ${side}-msg">
          <div class="msg-img" style="background-image: url(${img})"></div>

          <div class="msg-bubble">
            <div class="msg-info">
              <div class="msg-info-name">${name}</div>
              <div class="msg-info-time">${formatDate(new Date())}</div>
            </div>

            <div class="msg-text">${text}</div>
          </div>
        </div>
      `;

      msgerChat.insertAdjacentHTML("beforeend", msgHTML);
      msgerChat.scrollTop += 500;
    }

    function botResponse() {
      const r = random(0, BOT_MSGS.length - 1);
      const msgText = BOT_MSGS[r];
      const delay = msgText.split(" ").length * 100;

      setTimeout(() => {
        appendMessage(BOT_NAME, BOT_IMG, "left", msgText);
      }, delay);
    }

    // Utils
    function get(selector, root = document) {
      return root.querySelector(selector);
    }

    function formatDate(date) {
      const h = "0" + date.getHours();
      const m = "0" + date.getMinutes();

      return `${h.slice(-2)}:${m.slice(-2)}`;
    }

    function random(min, max) {
      return Math.floor(Math.random() * (max - min) + min);
    }
});
</script>
    </body>
</html>